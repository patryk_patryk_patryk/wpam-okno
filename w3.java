package com.patryk_szczepanski;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class w3 extends AppCompatActivity {

    private CheckBox checkBox100, checkBox101, checkBox102, checkBox103, checkBox104, checkBox105, checkBox106, checkBox107, checkBox108, checkBox109;
    private CheckBox checkBox110, checkBox111, checkBox112, checkBox113, checkBox114, checkBox115, checkBox116, checkBox117, checkBox118, checkBox119;
    private CheckBox checkBox120, checkBox121, checkBox122, checkBox123, checkBox124, checkBox125, checkBox126, checkBox127, checkBox128, checkBox129;
    private CheckBox checkBox130, checkBox131, checkBox132, checkBox133, checkBox134, checkBox135, checkBox136, checkBox137, checkBox138, checkBox139;
    private CheckBox checkBox140, checkBox141, checkBox142, checkBox143, checkBox144, checkBox145;

    private static final String CHECK_BOX_KEY100 = "checkBoxState100";    private static final String CHECK_BOX_KEY101 = "checkBoxState101";    private static final String CHECK_BOX_KEY102 = "checkBoxState102";    private static final String CHECK_BOX_KEY103 = "checkBoxState103";
    private static final String CHECK_BOX_KEY104 = "checkBoxState104";    private static final String CHECK_BOX_KEY105 = "checkBoxState105";    private static final String CHECK_BOX_KEY106 = "checkBoxState106";    private static final String CHECK_BOX_KEY107 = "checkBoxState107";
    private static final String CHECK_BOX_KEY108 = "checkBoxState108";    private static final String CHECK_BOX_KEY109 = "checkBoxState109";    private static final String CHECK_BOX_KEY110 = "checkBoxState110";    private static final String CHECK_BOX_KEY111 = "checkBoxState111";
    private static final String CHECK_BOX_KEY112 = "checkBoxState112";    private static final String CHECK_BOX_KEY113 = "checkBoxState113";    private static final String CHECK_BOX_KEY114 = "checkBoxState114";    private static final String CHECK_BOX_KEY115 = "checkBoxState115";
    private static final String CHECK_BOX_KEY116 = "checkBoxState116";    private static final String CHECK_BOX_KEY117 = "checkBoxState117";    private static final String CHECK_BOX_KEY118 = "checkBoxState118";    private static final String CHECK_BOX_KEY119 = "checkBoxState119";
    private static final String CHECK_BOX_KEY120 = "checkBoxState120";    private static final String CHECK_BOX_KEY121 = "checkBoxState121";    private static final String CHECK_BOX_KEY122 = "checkBoxState122";    private static final String CHECK_BOX_KEY123 = "checkBoxState123";
    private static final String CHECK_BOX_KEY124 = "checkBoxState124";    private static final String CHECK_BOX_KEY125 = "checkBoxState125";    private static final String CHECK_BOX_KEY126 = "checkBoxState126";    private static final String CHECK_BOX_KEY127 = "checkBoxState127";
    private static final String CHECK_BOX_KEY128 = "checkBoxState128";    private static final String CHECK_BOX_KEY129 = "checkBoxState129";    private static final String CHECK_BOX_KEY130 = "checkBoxState130";    private static final String CHECK_BOX_KEY131 = "checkBoxState131";
    private static final String CHECK_BOX_KEY132 = "checkBoxState132";    private static final String CHECK_BOX_KEY133 = "checkBoxState133";    private static final String CHECK_BOX_KEY134 = "checkBoxState134";    private static final String CHECK_BOX_KEY135 = "checkBoxState135";
    private static final String CHECK_BOX_KEY136 = "checkBoxState136";    private static final String CHECK_BOX_KEY137 = "checkBoxState137";    private static final String CHECK_BOX_KEY138 = "checkBoxState138";    private static final String CHECK_BOX_KEY139 = "checkBoxState139";
    private static final String CHECK_BOX_KEY140 = "checkBoxState140";    private static final String CHECK_BOX_KEY141 = "checkBoxState141";    private static final String CHECK_BOX_KEY142 = "checkBoxState142";    private static final String CHECK_BOX_KEY143 = "checkBoxState143";
    private static final String CHECK_BOX_KEY144 = "checkBoxState144";    private static final String CHECK_BOX_KEY145 = "checkBoxState145";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_w3);

        final SharedPreferences sharedPref = w3.this.getPreferences(Context.MODE_PRIVATE);

        boolean isMyValueChecked100 = sharedPref.getBoolean(CHECK_BOX_KEY100, false);        boolean isMyValueChecked101 = sharedPref.getBoolean(CHECK_BOX_KEY101, false);        boolean isMyValueChecked102 = sharedPref.getBoolean(CHECK_BOX_KEY102, false);        boolean isMyValueChecked103 = sharedPref.getBoolean(CHECK_BOX_KEY103, false);
        boolean isMyValueChecked104 = sharedPref.getBoolean(CHECK_BOX_KEY104, false);        boolean isMyValueChecked105 = sharedPref.getBoolean(CHECK_BOX_KEY105, false);        boolean isMyValueChecked106 = sharedPref.getBoolean(CHECK_BOX_KEY106, false);        boolean isMyValueChecked107 = sharedPref.getBoolean(CHECK_BOX_KEY107, false);
        boolean isMyValueChecked108 = sharedPref.getBoolean(CHECK_BOX_KEY108, false);        boolean isMyValueChecked109 = sharedPref.getBoolean(CHECK_BOX_KEY109, false);        boolean isMyValueChecked110 = sharedPref.getBoolean(CHECK_BOX_KEY110, false);        boolean isMyValueChecked111 = sharedPref.getBoolean(CHECK_BOX_KEY111, false);
        boolean isMyValueChecked112 = sharedPref.getBoolean(CHECK_BOX_KEY112, false);        boolean isMyValueChecked113 = sharedPref.getBoolean(CHECK_BOX_KEY113, false);        boolean isMyValueChecked114 = sharedPref.getBoolean(CHECK_BOX_KEY114, false);        boolean isMyValueChecked115 = sharedPref.getBoolean(CHECK_BOX_KEY115, false);
        boolean isMyValueChecked116 = sharedPref.getBoolean(CHECK_BOX_KEY116, false);        boolean isMyValueChecked117 = sharedPref.getBoolean(CHECK_BOX_KEY117, false);        boolean isMyValueChecked118 = sharedPref.getBoolean(CHECK_BOX_KEY118, false);        boolean isMyValueChecked119 = sharedPref.getBoolean(CHECK_BOX_KEY119, false);
        boolean isMyValueChecked120 = sharedPref.getBoolean(CHECK_BOX_KEY120, false);        boolean isMyValueChecked121 = sharedPref.getBoolean(CHECK_BOX_KEY121, false);        boolean isMyValueChecked122 = sharedPref.getBoolean(CHECK_BOX_KEY122, false);        boolean isMyValueChecked123 = sharedPref.getBoolean(CHECK_BOX_KEY123, false);
        boolean isMyValueChecked124 = sharedPref.getBoolean(CHECK_BOX_KEY124, false);        boolean isMyValueChecked125 = sharedPref.getBoolean(CHECK_BOX_KEY125, false);        boolean isMyValueChecked126 = sharedPref.getBoolean(CHECK_BOX_KEY126, false);        boolean isMyValueChecked127 = sharedPref.getBoolean(CHECK_BOX_KEY127, false);
        boolean isMyValueChecked128 = sharedPref.getBoolean(CHECK_BOX_KEY128, false);        boolean isMyValueChecked129 = sharedPref.getBoolean(CHECK_BOX_KEY129, false);        boolean isMyValueChecked130 = sharedPref.getBoolean(CHECK_BOX_KEY130, false);        boolean isMyValueChecked131 = sharedPref.getBoolean(CHECK_BOX_KEY131, false);
        boolean isMyValueChecked132 = sharedPref.getBoolean(CHECK_BOX_KEY132, false);        boolean isMyValueChecked133 = sharedPref.getBoolean(CHECK_BOX_KEY133, false);        boolean isMyValueChecked134 = sharedPref.getBoolean(CHECK_BOX_KEY134, false);        boolean isMyValueChecked135 = sharedPref.getBoolean(CHECK_BOX_KEY135, false);
        boolean isMyValueChecked136 = sharedPref.getBoolean(CHECK_BOX_KEY136, false);        boolean isMyValueChecked137 = sharedPref.getBoolean(CHECK_BOX_KEY137, false);        boolean isMyValueChecked138 = sharedPref.getBoolean(CHECK_BOX_KEY138, false);        boolean isMyValueChecked139 = sharedPref.getBoolean(CHECK_BOX_KEY139, false);
        boolean isMyValueChecked140 = sharedPref.getBoolean(CHECK_BOX_KEY140, false);        boolean isMyValueChecked141 = sharedPref.getBoolean(CHECK_BOX_KEY141, false);        boolean isMyValueChecked142 = sharedPref.getBoolean(CHECK_BOX_KEY142, false);        boolean isMyValueChecked143 = sharedPref.getBoolean(CHECK_BOX_KEY143, false);
        boolean isMyValueChecked144 = sharedPref.getBoolean(CHECK_BOX_KEY144, false);        boolean isMyValueChecked145 = sharedPref.getBoolean(CHECK_BOX_KEY145, false);

        checkBox100 = (CheckBox) findViewById(R.id.checkBox100);        checkBox101 = (CheckBox) findViewById(R.id.checkBox101);        checkBox102 = (CheckBox) findViewById(R.id.checkBox102);        checkBox103 = (CheckBox) findViewById(R.id.checkBox103);
        checkBox104 = (CheckBox) findViewById(R.id.checkBox104);        checkBox105 = (CheckBox) findViewById(R.id.checkBox105);       checkBox106 = (CheckBox) findViewById(R.id.checkBox106);        checkBox107 = (CheckBox) findViewById(R.id.checkBox107);
        checkBox108 = (CheckBox) findViewById(R.id.checkBox108);        checkBox109 = (CheckBox) findViewById(R.id.checkBox109);        checkBox110 = (CheckBox) findViewById(R.id.checkBox110);        checkBox111 = (CheckBox) findViewById(R.id.checkBox111);
        checkBox112 = (CheckBox) findViewById(R.id.checkBox112);        checkBox113 = (CheckBox) findViewById(R.id.checkBox113);        checkBox114 = (CheckBox) findViewById(R.id.checkBox114);        checkBox115 = (CheckBox) findViewById(R.id.checkBox115);
        checkBox116 = (CheckBox) findViewById(R.id.checkBox116);        checkBox117 = (CheckBox) findViewById(R.id.checkBox117);        checkBox118 = (CheckBox) findViewById(R.id.checkBox118);        checkBox119 = (CheckBox) findViewById(R.id.checkBox119);
        checkBox120 = (CheckBox) findViewById(R.id.checkBox120);        checkBox121 = (CheckBox) findViewById(R.id.checkBox121);        checkBox122 = (CheckBox) findViewById(R.id.checkBox122);        checkBox123 = (CheckBox) findViewById(R.id.checkBox123);
        checkBox124 = (CheckBox) findViewById(R.id.checkBox124);        checkBox125 = (CheckBox) findViewById(R.id.checkBox125);        checkBox126 = (CheckBox) findViewById(R.id.checkBox126);        checkBox127 = (CheckBox) findViewById(R.id.checkBox127);
        checkBox128 = (CheckBox) findViewById(R.id.checkBox128);        checkBox129 = (CheckBox) findViewById(R.id.checkBox129);        checkBox130 = (CheckBox) findViewById(R.id.checkBox130);        checkBox131 = (CheckBox) findViewById(R.id.checkBox131);
        checkBox132 = (CheckBox) findViewById(R.id.checkBox132);        checkBox133 = (CheckBox) findViewById(R.id.checkBox133);        checkBox134 = (CheckBox) findViewById(R.id.checkBox134);        checkBox135 = (CheckBox) findViewById(R.id.checkBox135);
        checkBox136 = (CheckBox) findViewById(R.id.checkBox136);        checkBox137 = (CheckBox) findViewById(R.id.checkBox137);        checkBox138 = (CheckBox) findViewById(R.id.checkBox138);        checkBox139 = (CheckBox) findViewById(R.id.checkBox139);
        checkBox140 = (CheckBox) findViewById(R.id.checkBox140);        checkBox141 = (CheckBox) findViewById(R.id.checkBox141);        checkBox142 = (CheckBox) findViewById(R.id.checkBox142);        checkBox143 = (CheckBox) findViewById(R.id.checkBox143);
        checkBox144 = (CheckBox) findViewById(R.id.checkBox144);        checkBox145 = (CheckBox) findViewById(R.id.checkBox145);

        checkBox100.setChecked(isMyValueChecked100);        checkBox101.setChecked(isMyValueChecked101);        checkBox102.setChecked(isMyValueChecked102);        checkBox103.setChecked(isMyValueChecked103);
        checkBox104.setChecked(isMyValueChecked104);        checkBox105.setChecked(isMyValueChecked105);        checkBox106.setChecked(isMyValueChecked106);        checkBox107.setChecked(isMyValueChecked107);
        checkBox108.setChecked(isMyValueChecked108);        checkBox109.setChecked(isMyValueChecked109);        checkBox110.setChecked(isMyValueChecked110);        checkBox111.setChecked(isMyValueChecked111);
        checkBox112.setChecked(isMyValueChecked112);        checkBox113.setChecked(isMyValueChecked113);        checkBox114.setChecked(isMyValueChecked114);        checkBox115.setChecked(isMyValueChecked115);
        checkBox116.setChecked(isMyValueChecked116);        checkBox117.setChecked(isMyValueChecked117);        checkBox118.setChecked(isMyValueChecked118);        checkBox119.setChecked(isMyValueChecked119);
        checkBox120.setChecked(isMyValueChecked120);        checkBox121.setChecked(isMyValueChecked121);        checkBox122.setChecked(isMyValueChecked122);        checkBox123.setChecked(isMyValueChecked123);
        checkBox124.setChecked(isMyValueChecked124);        checkBox125.setChecked(isMyValueChecked125);        checkBox126.setChecked(isMyValueChecked126);        checkBox127.setChecked(isMyValueChecked127);
        checkBox128.setChecked(isMyValueChecked128);        checkBox129.setChecked(isMyValueChecked129);        checkBox130.setChecked(isMyValueChecked130);        checkBox131.setChecked(isMyValueChecked131);
        checkBox132.setChecked(isMyValueChecked132);        checkBox133.setChecked(isMyValueChecked133);        checkBox134.setChecked(isMyValueChecked134);        checkBox135.setChecked(isMyValueChecked135);
        checkBox136.setChecked(isMyValueChecked136);        checkBox137.setChecked(isMyValueChecked137);        checkBox138.setChecked(isMyValueChecked138);        checkBox139.setChecked(isMyValueChecked139);
        checkBox140.setChecked(isMyValueChecked140);        checkBox141.setChecked(isMyValueChecked141);        checkBox142.setChecked(isMyValueChecked142);        checkBox143.setChecked(isMyValueChecked143);
        checkBox144.setChecked(isMyValueChecked144);        checkBox145.setChecked(isMyValueChecked145);

        checkBox100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY100, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox101.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY101, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox102.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY102, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox103.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY103, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox104.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY104, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox105.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY105, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox106.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY106, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox107.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY107, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox108.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY108, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox109.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY109, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox110.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY110, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox111.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY111, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox112.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY112, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox113.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY113, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox114.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY114, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox115.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY115, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox116.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY116, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox117.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY117, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox118.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY118, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox119.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY119, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox120.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY120, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox121.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY121, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox122.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY122, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox123.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY123, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox124.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY124, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox125.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY125, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox126.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY126, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox127.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY127, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox128.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY128, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox129.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY129, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox130.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY130, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox131.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY131, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox132.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY132, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox133.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY133, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox134.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY134, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox135.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY135, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox136.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY136, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox137.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY137, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox138.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY138, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox139.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY139, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox140.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY140, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox141.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY141, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox142.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY142, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox143.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY143, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox144.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY144, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox145.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY145, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
    }
    public void click(View view) {
        Intent intent;
        switch (view.getId())
        {
            case R.id.button6:
                intent = new Intent(w3.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
