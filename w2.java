package com.patryk_szczepanski;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class w2 extends AppCompatActivity {

    private CheckBox checkBox44, checkBox45, checkBox46, checkBox47, checkBox48, checkBox49, checkBox50, checkBox51, checkBox52, checkBox53;
    private CheckBox checkBox54, checkBox55, checkBox56, checkBox57, checkBox58, checkBox59, checkBox60, checkBox61, checkBox62, checkBox63;
    private CheckBox checkBox64, checkBox65, checkBox66, checkBox67, checkBox68, checkBox69, checkBox70, checkBox71, checkBox72, checkBox73;
    private CheckBox checkBox74, checkBox75, checkBox76, checkBox77, checkBox78, checkBox79, checkBox80, checkBox81, checkBox82, checkBox83;
    private CheckBox checkBox84, checkBox85, checkBox86, checkBox87, checkBox88, checkBox89, checkBox90, checkBox91, checkBox92, checkBox93;
    private CheckBox checkBox94, checkBox95, checkBox96, checkBox97, checkBox98, checkBox99;

    private static final String CHECK_BOX_KEY44 = "checkBoxState44";  private static final String CHECK_BOX_KEY45 = "checkBoxState45";  private static final String CHECK_BOX_KEY46 = "checkBoxState46";  private static final String CHECK_BOX_KEY47 = "checkBoxState47"; private static final String CHECK_BOX_KEY48 = "checkBoxState48";
    private static final String CHECK_BOX_KEY49 = "checkBoxState49";  private static final String CHECK_BOX_KEY50 = "checkBoxState50";  private static final String CHECK_BOX_KEY51 = "checkBoxState51";  private static final String CHECK_BOX_KEY52 = "checkBoxState52"; private static final String CHECK_BOX_KEY53 = "checkBoxState53";
    private static final String CHECK_BOX_KEY54 = "checkBoxState54";  private static final String CHECK_BOX_KEY55 = "checkBoxState55";  private static final String CHECK_BOX_KEY56 = "checkBoxState56";  private static final String CHECK_BOX_KEY57 = "checkBoxState57"; private static final String CHECK_BOX_KEY58 = "checkBoxState58";
    private static final String CHECK_BOX_KEY59 = "checkBoxState59";  private static final String CHECK_BOX_KEY60 = "checkBoxState60";  private static final String CHECK_BOX_KEY61 = "checkBoxState61";  private static final String CHECK_BOX_KEY62 = "checkBoxState62"; private static final String CHECK_BOX_KEY63 = "checkBoxState63";
    private static final String CHECK_BOX_KEY64 = "checkBoxState64";  private static final String CHECK_BOX_KEY65 = "checkBoxState65";  private static final String CHECK_BOX_KEY66 = "checkBoxState66";  private static final String CHECK_BOX_KEY67 = "checkBoxState67"; private static final String CHECK_BOX_KEY68 = "checkBoxState68";
    private static final String CHECK_BOX_KEY69 = "checkBoxState69";  private static final String CHECK_BOX_KEY70 = "checkBoxState70";  private static final String CHECK_BOX_KEY71 = "checkBoxState71";  private static final String CHECK_BOX_KEY72 = "checkBoxState72"; private static final String CHECK_BOX_KEY73 = "checkBoxState73";
    private static final String CHECK_BOX_KEY74 = "checkBoxState74";  private static final String CHECK_BOX_KEY75 = "checkBoxState75";  private static final String CHECK_BOX_KEY76 = "checkBoxState76";  private static final String CHECK_BOX_KEY77 = "checkBoxState77"; private static final String CHECK_BOX_KEY78 = "checkBoxState78";
    private static final String CHECK_BOX_KEY79 = "checkBoxState79";  private static final String CHECK_BOX_KEY80 = "checkBoxState80";  private static final String CHECK_BOX_KEY81 = "checkBoxState81";  private static final String CHECK_BOX_KEY82 = "checkBoxState82"; private static final String CHECK_BOX_KEY83 = "checkBoxState83";
    private static final String CHECK_BOX_KEY84 = "checkBoxState84";  private static final String CHECK_BOX_KEY85 = "checkBoxState85";  private static final String CHECK_BOX_KEY86 = "checkBoxState86";  private static final String CHECK_BOX_KEY87 = "checkBoxState87"; private static final String CHECK_BOX_KEY88 = "checkBoxState88";
    private static final String CHECK_BOX_KEY89 = "checkBoxState89";  private static final String CHECK_BOX_KEY90 = "checkBoxState90";  private static final String CHECK_BOX_KEY91 = "checkBoxState91";  private static final String CHECK_BOX_KEY92 = "checkBoxState92"; private static final String CHECK_BOX_KEY93 = "checkBoxState93";
    private static final String CHECK_BOX_KEY94 = "checkBoxState94";  private static final String CHECK_BOX_KEY95 = "checkBoxState95";  private static final String CHECK_BOX_KEY96 = "checkBoxState96";  private static final String CHECK_BOX_KEY97 = "checkBoxState97"; private static final String CHECK_BOX_KEY98 = "checkBoxState97";
    private static final String CHECK_BOX_KEY99 = "checkBoxState99";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_w2);

        final SharedPreferences sharedPref = w2.this.getPreferences(Context.MODE_PRIVATE);

        boolean isMyValueChecked44 = sharedPref.getBoolean(CHECK_BOX_KEY44, false); boolean isMyValueChecked45 = sharedPref.getBoolean(CHECK_BOX_KEY45, false); boolean isMyValueChecked46 = sharedPref.getBoolean(CHECK_BOX_KEY46, false); boolean isMyValueChecked47 = sharedPref.getBoolean(CHECK_BOX_KEY47, false); boolean isMyValueChecked48 = sharedPref.getBoolean(CHECK_BOX_KEY48, false);
        boolean isMyValueChecked49 = sharedPref.getBoolean(CHECK_BOX_KEY49, false); boolean isMyValueChecked50 = sharedPref.getBoolean(CHECK_BOX_KEY50, false); boolean isMyValueChecked51 = sharedPref.getBoolean(CHECK_BOX_KEY51, false); boolean isMyValueChecked52 = sharedPref.getBoolean(CHECK_BOX_KEY52, false); boolean isMyValueChecked53 = sharedPref.getBoolean(CHECK_BOX_KEY53, false);
        boolean isMyValueChecked54 = sharedPref.getBoolean(CHECK_BOX_KEY54, false); boolean isMyValueChecked55 = sharedPref.getBoolean(CHECK_BOX_KEY55, false); boolean isMyValueChecked56 = sharedPref.getBoolean(CHECK_BOX_KEY56, false); boolean isMyValueChecked57 = sharedPref.getBoolean(CHECK_BOX_KEY57, false); boolean isMyValueChecked58 = sharedPref.getBoolean(CHECK_BOX_KEY58, false);
        boolean isMyValueChecked59 = sharedPref.getBoolean(CHECK_BOX_KEY59, false); boolean isMyValueChecked60 = sharedPref.getBoolean(CHECK_BOX_KEY60, false); boolean isMyValueChecked61 = sharedPref.getBoolean(CHECK_BOX_KEY61, false); boolean isMyValueChecked62 = sharedPref.getBoolean(CHECK_BOX_KEY62, false); boolean isMyValueChecked63 = sharedPref.getBoolean(CHECK_BOX_KEY63, false);
        boolean isMyValueChecked64 = sharedPref.getBoolean(CHECK_BOX_KEY64, false); boolean isMyValueChecked65 = sharedPref.getBoolean(CHECK_BOX_KEY65, false); boolean isMyValueChecked66 = sharedPref.getBoolean(CHECK_BOX_KEY66, false); boolean isMyValueChecked67 = sharedPref.getBoolean(CHECK_BOX_KEY67, false); boolean isMyValueChecked68 = sharedPref.getBoolean(CHECK_BOX_KEY68, false);
        boolean isMyValueChecked69 = sharedPref.getBoolean(CHECK_BOX_KEY69, false); boolean isMyValueChecked70 = sharedPref.getBoolean(CHECK_BOX_KEY70, false); boolean isMyValueChecked71 = sharedPref.getBoolean(CHECK_BOX_KEY71, false); boolean isMyValueChecked72 = sharedPref.getBoolean(CHECK_BOX_KEY72, false); boolean isMyValueChecked73 = sharedPref.getBoolean(CHECK_BOX_KEY73, false);
        boolean isMyValueChecked74 = sharedPref.getBoolean(CHECK_BOX_KEY74, false); boolean isMyValueChecked75 = sharedPref.getBoolean(CHECK_BOX_KEY75, false); boolean isMyValueChecked76 = sharedPref.getBoolean(CHECK_BOX_KEY76, false); boolean isMyValueChecked77 = sharedPref.getBoolean(CHECK_BOX_KEY77, false); boolean isMyValueChecked78 = sharedPref.getBoolean(CHECK_BOX_KEY78, false);
        boolean isMyValueChecked79 = sharedPref.getBoolean(CHECK_BOX_KEY79, false); boolean isMyValueChecked80 = sharedPref.getBoolean(CHECK_BOX_KEY80, false); boolean isMyValueChecked81 = sharedPref.getBoolean(CHECK_BOX_KEY81, false); boolean isMyValueChecked82 = sharedPref.getBoolean(CHECK_BOX_KEY82, false); boolean isMyValueChecked83 = sharedPref.getBoolean(CHECK_BOX_KEY83, false);
        boolean isMyValueChecked84 = sharedPref.getBoolean(CHECK_BOX_KEY84, false); boolean isMyValueChecked85 = sharedPref.getBoolean(CHECK_BOX_KEY85, false); boolean isMyValueChecked86 = sharedPref.getBoolean(CHECK_BOX_KEY86, false); boolean isMyValueChecked87 = sharedPref.getBoolean(CHECK_BOX_KEY87, false); boolean isMyValueChecked88 = sharedPref.getBoolean(CHECK_BOX_KEY88, false);
        boolean isMyValueChecked89 = sharedPref.getBoolean(CHECK_BOX_KEY89, false); boolean isMyValueChecked90 = sharedPref.getBoolean(CHECK_BOX_KEY90, false); boolean isMyValueChecked91 = sharedPref.getBoolean(CHECK_BOX_KEY91, false); boolean isMyValueChecked92 = sharedPref.getBoolean(CHECK_BOX_KEY92, false); boolean isMyValueChecked93 = sharedPref.getBoolean(CHECK_BOX_KEY93, false);
        boolean isMyValueChecked94 = sharedPref.getBoolean(CHECK_BOX_KEY94, false); boolean isMyValueChecked95 = sharedPref.getBoolean(CHECK_BOX_KEY95, false); boolean isMyValueChecked96 = sharedPref.getBoolean(CHECK_BOX_KEY96, false); boolean isMyValueChecked97 = sharedPref.getBoolean(CHECK_BOX_KEY97, false); boolean isMyValueChecked98 = sharedPref.getBoolean(CHECK_BOX_KEY98, false);
        boolean isMyValueChecked99 = sharedPref.getBoolean(CHECK_BOX_KEY99, false);

        checkBox44 = (CheckBox) findViewById(R.id.checkBox44);        checkBox45 = (CheckBox) findViewById(R.id.checkBox45);        checkBox46 = (CheckBox) findViewById(R.id.checkBox46);        checkBox47 = (CheckBox) findViewById(R.id.checkBox47);        checkBox48 = (CheckBox) findViewById(R.id.checkBox48);
        checkBox49 = (CheckBox) findViewById(R.id.checkBox49);        checkBox50 = (CheckBox) findViewById(R.id.checkBox50);        checkBox51 = (CheckBox) findViewById(R.id.checkBox51);        checkBox52 = (CheckBox) findViewById(R.id.checkBox52);        checkBox53 = (CheckBox) findViewById(R.id.checkBox53);
        checkBox54 = (CheckBox) findViewById(R.id.checkBox54);        checkBox55 = (CheckBox) findViewById(R.id.checkBox55);        checkBox56 = (CheckBox) findViewById(R.id.checkBox56);        checkBox57 = (CheckBox) findViewById(R.id.checkBox57);        checkBox58 = (CheckBox) findViewById(R.id.checkBox58);
        checkBox59 = (CheckBox) findViewById(R.id.checkBox59);        checkBox60 = (CheckBox) findViewById(R.id.checkBox60);        checkBox61 = (CheckBox) findViewById(R.id.checkBox61);        checkBox62 = (CheckBox) findViewById(R.id.checkBox62);        checkBox63 = (CheckBox) findViewById(R.id.checkBox63);
        checkBox64 = (CheckBox) findViewById(R.id.checkBox64);        checkBox65 = (CheckBox) findViewById(R.id.checkBox65);        checkBox66 = (CheckBox) findViewById(R.id.checkBox66);        checkBox67 = (CheckBox) findViewById(R.id.checkBox67);        checkBox68 = (CheckBox) findViewById(R.id.checkBox68);
        checkBox69 = (CheckBox) findViewById(R.id.checkBox69);        checkBox70 = (CheckBox) findViewById(R.id.checkBox70);        checkBox71 = (CheckBox) findViewById(R.id.checkBox71);        checkBox72 = (CheckBox) findViewById(R.id.checkBox72);        checkBox73 = (CheckBox) findViewById(R.id.checkBox73);
        checkBox74 = (CheckBox) findViewById(R.id.checkBox74);        checkBox75 = (CheckBox) findViewById(R.id.checkBox75);        checkBox76 = (CheckBox) findViewById(R.id.checkBox76);        checkBox77 = (CheckBox) findViewById(R.id.checkBox77);        checkBox78 = (CheckBox) findViewById(R.id.checkBox78);
        checkBox79 = (CheckBox) findViewById(R.id.checkBox79);        checkBox80 = (CheckBox) findViewById(R.id.checkBox80);        checkBox81 = (CheckBox) findViewById(R.id.checkBox81);        checkBox82 = (CheckBox) findViewById(R.id.checkBox82);        checkBox83 = (CheckBox) findViewById(R.id.checkBox83);
        checkBox84 = (CheckBox) findViewById(R.id.checkBox84);        checkBox85 = (CheckBox) findViewById(R.id.checkBox85);        checkBox86 = (CheckBox) findViewById(R.id.checkBox86);        checkBox87 = (CheckBox) findViewById(R.id.checkBox87);        checkBox88 = (CheckBox) findViewById(R.id.checkBox88);
        checkBox89 = (CheckBox) findViewById(R.id.checkBox89);        checkBox90 = (CheckBox) findViewById(R.id.checkBox90);        checkBox91 = (CheckBox) findViewById(R.id.checkBox91);        checkBox92 = (CheckBox) findViewById(R.id.checkBox92);        checkBox93 = (CheckBox) findViewById(R.id.checkBox93);
        checkBox94 = (CheckBox) findViewById(R.id.checkBox94);        checkBox95 = (CheckBox) findViewById(R.id.checkBox95);        checkBox96 = (CheckBox) findViewById(R.id.checkBox96);        checkBox97 = (CheckBox) findViewById(R.id.checkBox97);        checkBox98 = (CheckBox) findViewById(R.id.checkBox98);
        checkBox99 = (CheckBox) findViewById(R.id.checkBox99);

        checkBox44.setChecked(isMyValueChecked44);
        checkBox45.setChecked(isMyValueChecked45);        checkBox46.setChecked(isMyValueChecked46);        checkBox47.setChecked(isMyValueChecked47);        checkBox48.setChecked(isMyValueChecked48);        checkBox49.setChecked(isMyValueChecked49);
        checkBox50.setChecked(isMyValueChecked50);        checkBox51.setChecked(isMyValueChecked51);        checkBox52.setChecked(isMyValueChecked52);        checkBox53.setChecked(isMyValueChecked53);        checkBox54.setChecked(isMyValueChecked54);
        checkBox55.setChecked(isMyValueChecked55);        checkBox56.setChecked(isMyValueChecked56);        checkBox57.setChecked(isMyValueChecked57);        checkBox58.setChecked(isMyValueChecked58);        checkBox59.setChecked(isMyValueChecked59);
        checkBox60.setChecked(isMyValueChecked60);        checkBox61.setChecked(isMyValueChecked61);        checkBox62.setChecked(isMyValueChecked62);        checkBox63.setChecked(isMyValueChecked63);        checkBox64.setChecked(isMyValueChecked64);
        checkBox65.setChecked(isMyValueChecked65);        checkBox66.setChecked(isMyValueChecked66);        checkBox67.setChecked(isMyValueChecked67);        checkBox68.setChecked(isMyValueChecked68);        checkBox69.setChecked(isMyValueChecked69);
        checkBox70.setChecked(isMyValueChecked70);        checkBox71.setChecked(isMyValueChecked71);        checkBox72.setChecked(isMyValueChecked72);        checkBox73.setChecked(isMyValueChecked73);        checkBox74.setChecked(isMyValueChecked74);
        checkBox75.setChecked(isMyValueChecked75);        checkBox76.setChecked(isMyValueChecked76);        checkBox77.setChecked(isMyValueChecked77);        checkBox78.setChecked(isMyValueChecked78);        checkBox79.setChecked(isMyValueChecked79);
        checkBox80.setChecked(isMyValueChecked80);        checkBox81.setChecked(isMyValueChecked81);        checkBox82.setChecked(isMyValueChecked82);        checkBox83.setChecked(isMyValueChecked83);        checkBox84.setChecked(isMyValueChecked84);
        checkBox85.setChecked(isMyValueChecked85);        checkBox86.setChecked(isMyValueChecked86);        checkBox87.setChecked(isMyValueChecked87);        checkBox88.setChecked(isMyValueChecked88);        checkBox89.setChecked(isMyValueChecked89);
        checkBox90.setChecked(isMyValueChecked90);        checkBox91.setChecked(isMyValueChecked91);        checkBox92.setChecked(isMyValueChecked92);        checkBox93.setChecked(isMyValueChecked93);        checkBox94.setChecked(isMyValueChecked94);
        checkBox95.setChecked(isMyValueChecked95);        checkBox96.setChecked(isMyValueChecked96);        checkBox97.setChecked(isMyValueChecked97);        checkBox98.setChecked(isMyValueChecked98);        checkBox99.setChecked(isMyValueChecked99);

        checkBox44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY44, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY45, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox46.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY46, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox47.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY47, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox48.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY48, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox49.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY49, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY50, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY51, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox52.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY52, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox53.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY53, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox54.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY54, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox55.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY55, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox56.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY56, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox57.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY57, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox58.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY58, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox59.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY59, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox60.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY60, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY61, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox62.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY62, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox63.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY63, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox64.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY64, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox65.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY65, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox66.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY66, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox67.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY67, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox68.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY68, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox69.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY69, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox70.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY70, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox71.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY71, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox72.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY72, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox73.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY73, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox74.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY74, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox75.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY75, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox76.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY76, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox77.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY77, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox78.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY78, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox79.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY79, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox80.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY80, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox81.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY81, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox82.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY82, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox83.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY83, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox84.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY84, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox85.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY85, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox86.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY86, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox87.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY87, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox88.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY88, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox89.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY89, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox90.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY90, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox91.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY91, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox92.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY92, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox93.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY93, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox94.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY94, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox95.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY95, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox96.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY96, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox97.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY97, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox98.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY98, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
        checkBox99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(CHECK_BOX_KEY99, ((CheckBox) v).isChecked());
                editor.commit();
            }
        });
    }
    public void click(View view) {
        Intent intent;
        switch (view.getId())
        {
            case R.id.button5:
                intent = new Intent(w2.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

}
